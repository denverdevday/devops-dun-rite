# DevOps Dun Rite

Code for talk initially given at [Denver Dev Day](https://denverdevday.github.io) - Oct 19 2018

Resources:
- [The Presentation Slides](https://raelyard.github.io/DevOpsDunRite/index.html)
- [The Presentation Repository README with links from the presentation](https://github.com/raelyard/DevOpsDunRite/blob/gh-pages/README.md)
